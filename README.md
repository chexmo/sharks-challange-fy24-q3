# sharks-challenge-FY24-Q3

### Virtual Horse Thread - Carrera de Caballos

#### Quick links

- [Consigna resumida de Virtual Horses](#resumen-de-la-consigna)
- [Consigna detallada de Virtual Horses](CONSIGNA.md)
- [Consideraciones del desarrollador](#consideraciones-del-desarrollador)
- [Información sobre el curso de Udemy](CURSO.md)
- [Contribuidores](CONTRIBUTING.md)

## Resumen de la consigna

La consigna general de la actual cápsula es resolver el problema titulado Virtual Horse Thread basándose en lo aprendido
a partir del curso de Udemy propuesto, el cual cubre los temas de concurrencia y performance en java.
Dicho problema, en pocas palabras, consiste en simular una carrera de en caballos con ciertas consideraciones, donde los
caballos deben poder procesarse de manera paralela y respetar ciertas normas.

- [Consigna detallada de Virtual Horses](CONSIGNA.md)
- [Información sobre el curso de Udemy](CURSO.md)

## Consideraciones del desarrollador

Como desarrollador de la aplicación he tomado las siguientes
consideraciones:

- Se prefieren los principios KISS y YAGNI por sobre SOLID para este problema
    - No es un proyecto productivo y una vez alcanzada cierto nivel de contento con la resolución no se planea ofrecer
      mantenimiento o escalabilidad.


- En lo posible simplificar los aspectos del problema que no sean evaluados hasta completar la consigna.
    - Esto incluye no usar frameworks o
      herramientas avanzadas mientras no es necesario. P. ej.: No usar ningún tipo de persistencia si no es necesario.
    - Una vez completada la consigna y si se puede, se agregarán interfaces adecuadas. P. ej.: Una interfaz gráfica o
      una API

