# Curso

El curso recomendado para esta cápsula es el siguiente:

* [Java multithreading (link Udemy personal)](https://www.udemy.com/course/java-multithreading-concurrency-performance-optimization/)
* [Java multithreading (link para Accenture Business Udemy)](https://accenture-ar.udemy.com/course/java-multithreading-concurrency-performance-optimization)

Como facilidad les ofrezco accesos rápidos a los ejemplos del curso
(el sXXy del principio indica a que sección del curso pertenece)

* [s02a_thread_creation](udemy-course/src/main/java/s02a_thread_creation)
* [s02b_thread_creation](udemy-course/src/main/java/s02b_thread_creation)
* [s03a_thread_termination](udemy-course/src/main/java/s03a_thread_termination)
* [s03b_joining_threads](udemy-course/src/main/java/s03b_joining_threads)
* [s04a_optimizing_latency](udemy-course/src/main/java/s04a_optimizing_latency)
* [s04b_optimizing_throughput](udemy-course/src/main/java/s04b_optimizing_throughput)
* [s05a_critical_section](udemy-course/src/main/java/s05a_critical_section)
* [s06a_synchronized](udemy-course/src/main/java/s06a_synchronized)
* [s06b_metrics](udemy-course/src/main/java/s06b_metrics)
* [s06c_data_race](udemy-course/src/main/java/s06c_data_race)
* [s06d_dead_locks](udemy-course/src/main/java/s06d_dead_locks)
* [s07a_reentrant_lock](udemy-courses-fx/src/main/java/s07a_reentrant_lock)
* [s07b_read_write_lock](udemy-course/src/main/java/s07b_read_write_lock)
* [s08a_wait_notify](udemy-course/src/main/java/s08a_wait_notify)
* [s09_atomic_integer](udemy-course/src/main/java/s09_atomic_integer)
* [s09b_atomic_reference](udemy-course/src/main/java/s09b_atomic_reference)
* [s10a_io_bound_app](udemy-course/src/main/java/s10a_io_bound_app)
* [s11a_virtual_threads](udemy-course/src/main/java/s11a_virtual_threads)
* [s11b_high_performance_io](udemy-course/src/main/java/s11b_high_performance_io)