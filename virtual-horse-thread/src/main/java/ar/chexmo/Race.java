package ar.chexmo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Naturalmente, todo el problema está enmarcado en el contexto de una (o n) carrera(s).
 * Esta clase provee este contexto y coordinación entre los elementos.
 */
public class Race {

    public static final int RACE_LENGTH = 100; // TODO esto debería ser 1000 pero lo reduje a 100 para agilizar el desarrollo

    private final List<Horse> horses = new ArrayList<>();
    private final Set<Thread> threads = new HashSet<>();
    private final List<Horse> winners = new ArrayList<>();
    private final int numberOfHorses;

    private final Lock winnerslock = new ReentrantLock();
    private final Condition winnersCondition = winnerslock.newCondition();

    public Race(int numberOfHorses) {
        this.numberOfHorses = numberOfHorses;
    }

    public void ready() {
        for (int i = 0; i < numberOfHorses; i++) {
            var name = String.format("Horse_%03d", i);
            var speed = (int) (1 + (Math.random() * 3));
            var stamina = (int) (1 + (Math.random() * 3));
            horses.add(new Horse(name, speed, stamina));
        }
    }

    public void set() {
        threads.clear();

        horses.forEach(horse -> threads.add(
                new Thread(() -> {
                    while (horse.getPosition() < RACE_LENGTH) {
                        horse.goForward();
                        horse.doWait();

                        System.out.println(horse);

                        if (horse.getPosition() > RACE_LENGTH && !winners.contains(horse)) {
                            winnerslock.lock();
                            try {
                                winners.add(horse);
                                winnersCondition.signal();
                            } finally {
                                winnerslock.unlock();
                            }
                        }
                    }
                }))
        );
    }

    public void go() {
        threads.forEach(Thread::start);

        while (winners.size() < 3) {
            waitAnyHorseEnds();
        }

        threads.forEach(Thread::interrupt);

        printWinners();
    }

    private void waitAnyHorseEnds() {
        winnerslock.lock();
        try {
            winnersCondition.await();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } finally {
            winnerslock.unlock();
        }
    }


    private void printWinners() {
        // FIXME sin estos sleep los println no se ejecutan.
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        System.out.println("====================      FINISH      =====================");
        System.out.println();
        System.out.println("\t\t\t\t\t\t\t" + winners.get(0).getName());
        System.out.println("\t\t" + winners.get(1).getName() + "\t\t||||||||||||||||");
        System.out.println("\t||||||||||||||||\t||||||||||||||||\t\t" + winners.get(2).getName());
        System.out.println("\t||||||||||||||||\t||||||||||||||||\t||||||||||||||||\t");

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

}
