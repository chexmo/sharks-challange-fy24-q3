package ar.chexmo;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        System.out.println("\n\n\n\n\n\n\n"); // cheap CLS
        System.out.println("=======================================================================");
        System.out.println("===============  Bienvenido a la carrera de caballos  =================");
        System.out.println("=======================================================================");

        System.out.print("\n\t* Ingrese la cantidad de corredores:\t");
        var sc = new Scanner(System.in);
        var racersAmount = sc.nextInt();

        Race race = new Race(racersAmount);

        System.out.println("==============================   READY   ==============================");
        race.ready();
        Thread.sleep(200);

        System.out.println("==============================    SET    ==============================");
        race.set();
        Thread.sleep(200);

        System.out.println("==============================    GO!    ==============================");
        race.go();

        System.out.println("==============================  All done  =============================");
    }

}