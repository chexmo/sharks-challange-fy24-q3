package ar.chexmo;

import java.util.Random;

public class Horse {

    private final String name;
    private final int speed;
    private final int stamina;

    private int position = 0;

    public Horse(String name, int speed, int stamina) {
        this.name = name;
        this.speed = speed;
        this.stamina = stamina;
    }

    public void goForward() {
        position += speed * new Random().nextInt(5) + 1;
    }

    public void doWait() {
        var waitTime = stamina - new Random().nextInt(5) + 1;
        if (waitTime > 0) {
            try {
                Thread.sleep(waitTime * 1000L);
            } catch (InterruptedException ignored) {
            }
        }
    }

    public String getName() {
        return name;
    }

    public int getPosition() {
        return position;
    }

    @Override
    public String toString() {
        return "Horse{" +
                "name='" + name + '\'' +
                ", speed=" + speed +
                ", stamina=" + stamina +
                ", position=" + position +
                '}';
    }

}
